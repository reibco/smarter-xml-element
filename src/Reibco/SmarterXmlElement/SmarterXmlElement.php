<?php
namespace Reibco\SmarterXmlElement;

use SimpleXMLElement;

class SmarterXmlElement extends SimpleXMLElement
{

    /**
     * Get the value of an XML element.
     *
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function value($type = 'string', $default = null)
    {
        if ($this->missing()) {
            return $default;
        }

        $value = (string) $this;

        try {
            settype($value, $type);
        } catch (ErrorException $e) {
            throw new ErrorException('Invalid target type for SmarterXMLElement value.', $e->getCode(), $e->getSeverity(), $e->getFile(), $e->getLine(), $e);
        }

        return $value;
    }

    /**
     * Get an attribute value on an XML element.
     *
     * @param string $name
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function attribute($name, $type = 'string', $default = null)
    {
        return $this->attributeNS($name, null, false, $type, $default);
    }

    /**
     * Get a namespaced attribute value on an XML element.
     *
     * @param string $name
     * @param string $ns
     * @param boolean $is_prefix
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function attributeNS($name, $ns = null, $is_prefix = false, $type = 'string', $default = null)
    {
        return $this->attributes($ns, $is_prefix)[$name]->value($type, $default);
    }

    /**
     * If the element exists in the XML document.
     *
     * @return boolean
     */
    public function exists()
    {
        return $this->getName() > '';
    }

    /**
     * If the element does not exist in the XML document.
     *
     * @return boolean
     */
    public function missing()
    {
        return !$this->exists();
    }

    /**
     * If the element is empty.
     *
     * @param boolean $trim
     * @return boolean
     */
    public function blank($trim = true)
    {
        $data = $this->value();
        if ($trim) {
            $data = trim($data);
        }
        return $data == '';
    }

    /**
     * Get the value of an XML element.
     *
     * @param string $field
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function fieldValue($field, $type = 'string', $default = null)
    {
        return $this->$field->value($type, $default);
    }

    /**
     * Get an attribute value on an XML element.
     *
     * @param string $field
     * @param string $name
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function fieldAttribute($field, $name, $type = 'string', $default = null)
    {
        return $this->$field->attribute($name, $type, $default);
    }

    /**
     * Get a namespaced attribute value on an XML element.
     *
     * @param string $field
     * @param string $name
     * @param string $ns
     * @param boolean $is_prefix
     * @param string $type
     * @param mixed $default
     * @throws ErrorException
     * @return mixed
     */
    public function fieldAttributeNS($field, $name, $ns = null, $is_prefix = false, $type = 'string', $default = null)
    {
        return $this->$field->attributeNS($name, $ns, $is_prefix, $type, $default);
    }

    /**
     * If the element exists in the XML document.
     *
     * @param string $field
     * @return boolean
     */
    public function fieldExists($field)
    {
        return $this->$field->exists();
    }

    /**
     * If the element does not exist in the XML document.
     *
     * @param string $field
     * @return boolean
     */
    public function fieldMissing($field)
    {
        return $this->$field->missing();
    }

    /**
     * If the element is empty.
     *
     * @param string $field
     * @param boolean $trim
     * @return boolean
     */
    public function fieldBlank($field, $trim = true)
    {
        return $this->$field->blank($trim);
    }
}
